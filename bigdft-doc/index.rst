.. BigDFT-suite documentation master file, created by
   sphinx-quickstart on Sun Sep 30 14:58:47 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BigDFT-suite's documentation!
========================================

The `BigDFT-suite` project regroups the packages which are needed
to install, run and employ the BigDFT code for production calculations. BigDFT
is a suite of programs that work together to perform Density Functional
Theory calculations on a wide class of systems. A broad overview of the
code is available on the `main website <https://www.bigdft.org>`_. The
documentation here will guide you from installing the code to using it
for scientific studies.

Overview of BigDFT
------------------

.. toctree::
   :maxdepth: 2

   overview/functionality

.. toctree::
   :maxdepth: 2

   overview/package

.. toctree::
   :maxdepth: 2

   overview/publications

.. toctree::
   :maxdepth: 2

   overview/license


User Instructions
-----------------

.. toctree::
   :maxdepth: 2

   users/install

.. toctree::
   :maxdepth: 2

   users/guide

.. toctree::
   :maxdepth: 1

   PyBigDFT API <https://l_sim.gitlab.io/bigdft-suite/PyBigDFT/build/html/index.html>

Developer Instructions
----------------------

.. toctree::
   :maxdepth: 2

   devel/developers

Old Website
-----------

The documentation is under construction and some content is still available
from the old website. See the link `old website <https://old.bigdft.org/Wiki/?title=BigDFT_website>`_.


Example of link  to :ref:`futile:futile_index`, followed by example
to :ref:`pybigdft:pybigdft_api`, actualized.


Example of link :py:class:`zipfile.ZipFile` to python class
or to :f:mod:`f/f_precisions` or maybe :py:func:`futile.Utils.find_files` this, or again :py:mod:`BigDFT.Logfiles`. If all these links are functional, we may reach the conclusion that *intersphinx* seems to work.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
