PyBigDFT Development Guide
==========================

PyBigDFT is the high level python interface which you can use to drive BigDFT
calculations. PyBigDFT does not directly interface with any Fortran routines,
and instead only interfaces using the file system. When you wish to add new
high level features to the code, this can be the ideal entry point.

The shared language between the Fortran code of BigDFT and python is the
YAML markup language. Thanks to Futile, BigDFT's Fortran can still flexibily
handle complex input. Python dictionaries can be easily converted into
YAML. For this reason, many of the PyBigDFT classes inherit from dictionary
so that they can easily be serialized to a yaml format. 

API Documentation
-----------------

When adding new functionality to PyBigDFT, you should document each 
module according to the `Google Doc String`_ style. This will ensure that the
API is automatically documented.

.. _Google Doc String: https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html

Regression Testing
------------------
Each module of PyBigDFT should include an `_example` subroutine which
demonstrates the functionality of the package. This example should strive
for as much code coverage as possible. After a new feature has been created,
and added to the `_example` routine, you can run the regression tests using
the following command:

.. code:: bash

    python -m unittest discover -s $PYTHONPATH/BigDFT/ -p "check_examples.py"

This command can be run in any directory after sourcing the BigDFT variables.
You will want to do this in a dedicated test directory as the results of
each test are written to files in the current directory. 

Notebook Demonstrations
-----------------------

Major functionality should be documented as a tutorial or lesson as a Jupyter
notebook. Once a notebook has been created, you should add it to the
`bigdft-doc` project.

.. todo:: Regression for the notebooks.
