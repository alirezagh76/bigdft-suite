variables:
  SDK_DOCKER_IMAGE: "bigdft/sdk:latest"

default:
  image: $SDK_DOCKER_IMAGE #ubuntu:latest

stages:
  - build
  - check
  - lint
  - deploy

Basic:
  image: ubuntu:latest
  stage: build
  script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y install autoconf autotools-dev automake git build-essential libblas-dev liblapack-dev curl wget gfortran libyaml-dev python3-numpy python3-yaml libopenmpi-dev python3-setuptools python3-pytest python3-pip cython3 pkg-config nvidia-cuda-toolkit ocl-icd-opencl-dev debhelper fakeroot cmake rsync
    - export JHBUILD_RUN_AS_ROOT="please do it"
    - python3 ./Installer.py -y autogen
    - mkdir tmp
    - cd tmp/
    - cp ../rcfiles/jhbuildrc jhbuildrc #Installer takes care of this
    - echo 'prefix = "/opt/bigdft-suite"' >> jhbuildrc
    - python3 ../Installer.py -y -f jhbuildrc build
    - python3 ../bundler/jhbuild.py -f jhbuildrc dist --dist-only bigdft-suite
    - apt -y install debhelper fakeroot
    - tar -xf bigdft-suite.tar.gz
    - cp -rp ../debian bigdft-suite/
    - export VERSION=$(head -n1 bigdft-suite/debian/changelog | sed "s/.*(\([0-9.]*\)-[0-9]*).*/\1/")
    - cp bigdft-suite.tar.gz bigdft-suite_${VERSION}.orig.tar.gz
    - mv bigdft-suite/ bigdft-suite-${VERSION}/
    - cd bigdft-suite-${VERSION}/
    - dpkg-buildpackage -rfakeroot
  artifacts:
    expire_in: 2 hour
    paths:
      - tmp/bigdft-suite.tar.gz
      - tmp/bigdft-suite_*
      # - futile
      # - tmp/futile
      # - atlab
      # - tmp/atlab
      # - psolver
      # - tmp/psolver
      # - bigdft
      # - tmp/bigdft

OpenCL:
  image: $SDK_DOCKER_IMAGE
  stage: build
  script:
    - ./Installer.py -y autogen
    - mkdir tmp
    - cd tmp/
    - ../Installer.py -y -f ../rcfiles/ubuntu_OCL.rc build
  artifacts:
    expire_in: 2 hour
    paths:
      - futile
      - tmp/futile
      - atlab
      - tmp/atlab
      - psolver
      - tmp/psolver
      - bigdft
      - tmp/bigdft
      - tmp/install


.test_environment_variables: &test_environment_variables
  - export OMPI_MCA_btl_vader_single_copy_mechanism=none
  - export OMP_NUM_THREADS=2
  - export run_parallel="mpirun -np 3 --allow-run-as-root --oversubscribe"
# LD_LIBRARY_PATH is for libbabel.so
  - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/bigdft-suite/lib

.check_library:
  stage: check
  before_script:
    - *test_environment_variables
    - source tmp/install/bin/bigdftvars.sh
  script:
    - cd tmp/${LIB_DIRECTORY}
    - make check
  artifacts:
    when: always
    paths:
      - tmp/${LIB_DIRECTORY}/tests

.check_directory:
  stage: check
  before_script:
    - *test_environment_variables
    - source tmp/install/bin/bigdftvars.sh
    - cd tmp/bigdft/tests/${TEST_DIRECTORY}
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/${TEST_DIRECTORY}

futile:
  extends: .check_library
  variables:
    LIB_DIRECTORY: futile

atlab:
  extends: .check_library
  variables:
    LIB_DIRECTORY: atlab

psolver:
  extends: .check_library
  variables:
    LIB_DIRECTORY: psolver

bigdft-cubic:
  extends: .check_directory
  variables:
    TEST_DIRECTORY: DFT/cubic
  script:
    - CHECK_MODE=short make check
  # stage: check
  # before_script:
  #   - *test_environment_variables
  # script:
  #   - cd tmp/bigdft/tests/DFT/cubic
  #   - CHECK_MODE=short make check
  # artifacts:
  #   when: always
  #   paths:
  #     - tmp/bigdft/tests/DFT/cubic
  retry: 1

bigdft-overDFT:
  stage: check
  before_script:
    - *test_environment_variables
  script:
    - cd tmp/bigdft/tests/overDFT
    - CHECK_MODE=short make check
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/overDFT
  retry: 1

bigdft-linear-minimal:
  stage: check
  before_script:
    - *test_environment_variables
  script:
    - cd tmp/bigdft/tests/DFT/linear
    - CHECK_MODE=custom make check checkonly_that="base periodic surface"
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/DFT/linear
  retry: 1

bigdft-linear-multipoles:
  stage: check
  before_script:
    - python3 -m pip install scipy
    - *test_environment_variables
    - cd tmp
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc build biopython
    - cd ../
    - source tmp/install/bin/bigdftvars.sh # employs the PyBigDFT of the client
  script:
    - cd tmp/bigdft/tests/DFT/linear
    - CHECK_MODE=custom make check checkonly_that="multipoles H2Omultipoles"
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/DFT/linear
  retry: 1

ntpoly-check:
  stage: check
  script:
    - cd tmp/bigdft/tests/DFT/linear
# LD_LIBRARY_PATH is for libbabel.so
    - OMP_NUM_THREADS=2 run_parallel="mpirun -np 3" LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CI_PROJECT_DIR}/tmp/install/lib CHECK_MODE=custom make check checkonly_that="bigpoly"
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/DFT/linear

bigdft-linear-extended:
  stage: check
  before_script:
    - *test_environment_variables
  script:
    - cd tmp/bigdft/tests/DFT/linear
    - CHECK_MODE=custom make check checkonly_that="directmin cdft rotatesmall verysparse"
  artifacts:
    when: always
    paths:
      - tmp/bigdft/tests/DFT/linear
  retry: 1

pybigdft-examples:
  stage: check
  before_script:
    - *test_environment_variables
    - source tmp/install/bin/bigdftvars.sh
    - export BIGDFT_MPIRUN="mpirun -np 3"
  script:
    - pip uninstall -y scipy
    - pip install scipy future py3Dmol
    - mkdir pybigdft-examples
    - cd pybigdft-examples
    - export SECOND_PYTHONPATH=`echo $PYTHONPATH | sed s/:/\ /g | awk '{print $2}'`
    - python -m unittest discover -s $SECOND_PYTHONPATH/BigDFT/ -p "check_examples.py" > out.txt
    - py.test --nbval --disable-warnings ../bigdft-doc/tutorials/Systems.ipynb
  artifacts:
    when: always
    paths:
      - pybigdft-examples
  retry: 1

pybigdft-nbtests:
  stage: check
  before_script:
    - *test_environment_variables
    - source tmp/install/bin/bigdftvars.sh
    - export BigDFT_loglvl="debug"
    - export BigDFT_logpath=${CI_PROJECT_DIR}/tmp_tests
  script:
    - mkdir tmp_tests
    - cd tmp_tests
    - echo $BigDFT_loglvl
    - echo $BigDFT_logpath
    - pytest --nbval-lax --disable-warnings --ignore=_ignore ${CI_PROJECT_DIR}/PyBigDFT/Tests/
  artifacts:
    when: always
    paths:
      - tmp_tests
  retry: 2  # remoterunner tests can run for variable times, producing differing outputs. This will trigger a false negative

Sphinx:
  image: $SDK_DOCKER_IMAGE
  stage: build
  script:
    - python3 bundler/jhbuild.py --conditions=+devdoc -f rcfiles/jhbuildrc  build bigdft-client
    - pip install guzzle-sphinx-theme sphinxcontrib-programoutput nbsphinx
    - sphinx-apidoc -fe -t ${CI_PROJECT_DIR}/PyBigDFT/source/templates -o ${CI_PROJECT_DIR}/PyBigDFT/source ${CI_PROJECT_DIR}/PyBigDFT/ *AiidaCalc* *setup.py
    - PYTHONPATH=$PWD/install/lib/python3.7/site-packages/ sphinx-build bigdft-doc build
    - mkdir ${CI_PROJECT_DIR}/coverage/
    - mv ${CI_PROJECT_DIR}/bigdft-doc/coverage.yaml ${CI_PROJECT_DIR}/coverage/
  artifacts:
    paths:
      - "PyBigDFT"
      - "futile"
      - "build"
      - "coverage"
  coverage: '/percentage.*?:(.*)/'

flake8:
  stage: lint
  script:
    - flake8 PyBigDFT/BigDFT/*.py --ignore=W291,W504,E121,E123,E126,E226,E24,E704
  allow_failure: true

pages:
  stage: deploy
  # only:
  #   - tags@l_sim/bigdft-suite
  before_script:
    - echo -n
  script:
    - mv build ${CI_PROJECT_DIR}/public/
    - mv PyBigDFT/build ${CI_PROJECT_DIR}/public/PyBigDFT
    - mv futile/build ${CI_PROJECT_DIR}/public/futile
    - mv tmp/bigdft-suite* ${CI_PROJECT_DIR}/public/
  artifacts:
    paths:
      - public
  needs: ["Basic", "OpenCL", "Sphinx"]  # deploy pages after build

client-build:
  image: ubuntu:latest
  stage: build
  script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y install autoconf autotools-dev automake git build-essential libblas-dev liblapack-dev curl wget gfortran libyaml-dev python3-numpy python3-yaml libopenmpi-dev python3-setuptools python3-pytest python3-pip cython3 pkg-config nvidia-cuda-toolkit ocl-icd-opencl-dev debhelper fakeroot cmake rsync
    - export JHBUILD_RUN_AS_ROOT="please do it"
    - python3 ./Installer.py -y autogen atlab
    - mkdir tmp_client
    - cd tmp_client/
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc build biopython
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc --conditions=+ase,+dill build bigdft-client
    - python3 ../bundler/jhbuild.py -f ../rcfiles/jhbuildrc dist --dist-only bigdft-client
  artifacts:
    expire_in: 2 hour
    paths:
      - tmp_client/install
      - tmp_client/futile
      - tmp_client/PyBigDFT
