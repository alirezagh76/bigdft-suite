"""
This module wraps the classes and functions you will need if you want to run
PyBigDFT operations on a remote machine.
"""

try:
    from BigDFT.AiidaCalculator import AiidaCalculator  # noqa F401
    AIIDA_FLAG = True
    from BigDFT.RemoteRunnerUtils import RemoteAiidaFunction
except ModuleNotFoundError:
    AIIDA_FLAG = False
from BigDFT.RemoteRunnerUtils import RemoteScript, \
    RemoteJSONFunction, RemoteDillFunction
from BigDFT.Calculators import Runner
from BigDFT.Datasets import Dataset
from BigDFT.URL import URL

import logging
from BigDFT.LogUtils import format_iterable
import os


# create and link logger
module_logger = logging.getLogger(__name__)


def _ensure_url(url, verbose=False):
    """make sure the URL is an actual class

    Arguments:
        url (str, URL):
            url to be enforced to BigDFT.URL

    Returns:
        URL
    """
    if url is None:
        return URL()

    if not isinstance(url, URL):
        # now require a URL handler for remote operations
        # create one if needed
        # warnings.DeprecationWarning is ignored by default, just print
        print('Warning: Explicit url expression is to be deprecated. '
              'Create a url using BigDFT.URL.')
        module_logger.warning('creating url class from string. Explicitly '
                              'create from BigDFT.URL() to be future-proof.')
        url = URL.from_string(url)

        url.verbose = verbose

    return url


class RemoteRunner(Runner, RemoteScript):
    """
    This class can be used to run python functions on a remote machine. This
    class combines the execution of a script with a python remote function.

    Args:

        function (func): the python function to be serialized.
            The return value of the function should be an object for which
            a relatively light-weight serialization is possible.
        name (str): the name of the remote function. Function name is employed
            if not provided.
        script (str): The script to be executed provided in string form.
            The result file of the script is assumed to be named
            `<name>-function-result`.
        submitter (str): the interpreter to be invoked. Should be, e.g.
            `bash` if the script is a shell script, or `qsub` if this is a
            submission script.
        url (str, URL): either a user@host string
            or URL() connection class (preferable)
        remote_dir (str): the path to the work directory on the remote machine.
           Should be associated to a writable directory.
        skip (bool): if true, we perform a lazy calculation.
        asynchronous (bool): If True, submit the calculation without waiting
           for the results.
        local_dir (str): local directory to prepare the IO files to send.
            The directory should exist and write permission should
            be granted.
        python (str): python interpreter to be invoked in the script.
        protocol (str): serialization method to be invoked for the function.
            can be 'JSON' of 'Dill', depending of the desired version.
        extra_encoder_functions (list)): list of dictionaries of the format
            {'cls': Class, 'func': function} which is employed to serialize
            non-instrinsic objects as well as non-numpy objects. Useful for
            the 'JSON' protocol.
        required_files (list): list of extra files that may be required for
            the good running of the function.
        output_files (list): list of the files that the function will produce
            that are supposed to be retrieved to the host computer.
        arguments (dict):  keyword arguments of the function.
            The arguments of the function should be serializable without
            the requirement of too much disk space. Such arguments cannot
            be named in the same way as the others.
        **kwargs (dict):  Further keyword arguments of the script,
            which will be substituted in the string representation.

    RemoteRunner exists to allow transfer of generic python functions to a
    remote system. Functions will be serialised via the user specified format
    and transferred over along with their arguments.

    Consider a simple function:

    >>> def test_func(arg):
    >>>     return arg

    This function can be run on a remote machine using RemoteRunner, as shown
    in the following example:

    Lets assume we have access to a remote machine with the ip address

    ``192.168.0.0``

    First, construct a URL connection to this machine using the URL module

    >>> from BigDFT.URL import URL
    >>> url = URL(user='user', host='192.168.0.0')

    Now give this, and the function as an argument to the RemoteRunner,
    along with any arguments required:

    >>> from BigDFT.RemoteRunners import RemoteRunner
    >>> remote_run = RemoteRunner(function = test_func,
    >>>                           url = url,
    >>>                           arguments={'arg':'This is a test argument'})

    Run your code with the ``run()`` method, then results can be retreived
    using the ``fetch_results()`` method. Whether or not a run has finished
    can be determined used the ``is_finished()`` method.

    >>> remote_run.run()
    >>> import time
    >>> # while we don't have a finished run, wait
    >>> while not remote_run.is_finished():
    >>>     time.sleep(1)
    >>> result = remote_run.fetch_result()
    >>> print(result)
    >>> 'This is a test argument'

    You now have the basic ingredients to begin running generic functions on
    remote machines. There is obviously more to these methods, but see their
    documentation for further details.
    """

    def __init__(self, function=None, submitter='bash', name=None,
                 url=None, skip=False, asynchronous=True, remote_dir='.',
                 rsync='', local_dir='/tmp',
                 script="#!/bin/bash\n", python='python', arguments=None,
                 protocol='JSON', extra_encoder_functions=None,
                 required_files=None, output_files=None, **kwargs):

        if arguments is None:
            arguments = {}
        if output_files is None:
            output_files = []
        if required_files is None:
            required_files = []
        if extra_encoder_functions is None:
            extra_encoder_functions = []

        # create logger and log message including init arguments
        self._logger = logging.getLogger(__name__ + '.RemoteRunner')
        logstr_exclude = ['self', '__class__']
        logstr = format_iterable(locals(), logstr_exclude)
        self._logger.info('create RemoteRunner with initial args: '+logstr)

        self.protocol = protocol.lower()  # remove case sensitivity
        # get verbose from kwargs, default True
        self.verbose = kwargs.get('verbose', True)
        url = _ensure_url(url, self.verbose)

        if rsync == '':  # update rsync based on verbosity if not specified
            self._logger.debug(f'no rsync set, and verbose is {self.verbose}')
            if self.verbose:
                rsync = '-auv'
            else:
                rsync = '-auq'
        url.rsync_flags = rsync
        self.url = url

        if function is None:  # function is always AiidaCalculator for aiida
            if self.protocol != 'aiida':
                raise ValueError('Function can only be '
                                 'None for AiiDA based runs')
            if name is None:  # set name if none specified
                name = 'AiiDA-calc'
        if name is None:
            name = function.__name__
        super().__init__(submitter=submitter, name=name, script=script,
                         result_file=name + '-function-result',
                         url=url, skip=skip, asynchronous=asynchronous,
                         remote_dir=remote_dir, local_dir=local_dir,
                         protocol=self.protocol, python=python,
                         extra_encoder_functions=extra_encoder_functions,
                         required_files=required_files, function=function,
                         arguments=arguments, output_files=output_files,
                         **kwargs)
        self.remote_function = self._create_remote_function(
            name, self._global_options)
        self.append_function(self.remote_function)

    def _create_remote_function(self, name, options):
        self._logger.debug(f'creating the remote function {name} w/ options: '
                           f'{format_iterable(options)}')
        rfargs = {'submitter': options['python'],
                  'name': name + '-function',
                  'function': options['function'],
                  'required_files': options['required_files']}
        protocol = options['protocol']

        dispatch = {'json': RemoteJSONFunction,
                    'dill': RemoteDillFunction}
        if AIIDA_FLAG:  # if aiida is in the env, add it to the dispatch table
            dispatch['aiida'] = RemoteAiidaFunction
        if protocol not in dispatch:
            raise ValueError('protocol must be one of',
                             list(dispatch.keys()))

        cls = dispatch[protocol]
        if protocol == 'json':
            rfargs.update(options['extra_encoder_functions'])
        if protocol == 'aiida':
            update = ['NMPIVAR', 'NOMPVAR']
            for key in [k for k in update if k in options]:
                print(f'passed key {key} to remote')
                rfargs[key] = options[key]

        # all the keys of the instantiated class so far are protected.
        self.protected_arguments = list(rfargs.keys())
        reargs = self._check_protected_arguments(options['arguments'])
        rfargs.update(reargs)
        return cls(**rfargs)

    def _check_protected_arguments(self, args):
        for arg in args:
            if arg in self.protected_arguments:
                raise ValueError("Keyword argument '" + arg +
                                 "' cannot be named this way (name clashing)")
        return args

    def pre_processing(self):
        """Ensure protected arguments, and gather the files to send."""
        reargs = self._check_protected_arguments(self.run_options['arguments'])
        self.remote_function.arguments = reargs
        self.url.ensure_dir(self.run_options['local_dir'], force_local=True)
        self.url.ensure_dir(self.run_options['remote_dir'])
        if hasattr(self, 'files_to_send'):
            files_to_send = self.files_to_send
        else:
            files_to_send = self.setup(dest=self.run_options['remote_dir'],
                                       src=self.run_options['local_dir'])

        if hasattr(self, 'files_have_been_received'):
            skip = self.remote_function.arguments.get('skip', None)
            force = self.remote_function.arguments.get('force', None)

            if force or (not skip):
                self._logger.debug('we are not skipping (or forcing a run), '
                                   'but files_have_been_received has been set'
                                   '. Deleting this attribute.')
                delattr(self, 'files_have_been_received')

        self._logger.debug('preprocessing completed with run_args: '
                           f'{format_iterable(self.remote_function.arguments)}'
                           )
        return {'files': files_to_send}

    def _get_opts(self, opts):
        return {key: self.run_options[key] for key in opts}

    def process_run(self, files):
        """
        Run the calculations.

        Most skip/force/async logic is evaluated here.
        """
        # allow passing of anyfile option, default to False
        anyfile = self.run_options.get('anyfile', True)
        # if force, disable skip
        force = self.run_options.get('force', False)
        if self.protocol == 'aiida':
            return self.remote_function.process_run(**self.local_options)
        if self.run_options['skip'] and not force and self.url.local:
            self._logger.debug('skip command active, checking '
                               'for finished run locally')
            if self.is_finished(remotely=False,
                                anyfile=anyfile,
                                verbose=False):
                # touch the files to prevent infinite is_finished() loops
                rfile = os.path.join(self.local_directory, self.resultfile)
                self._logger.debug(f'touching file {rfile} to prevent rerun')
                self.url.cmd(f'touch {rfile}')
                return {'status': 'finished_locally'}
        self.send_files(files)
        if self.run_options['skip'] and not force:
            self._logger.debug('skip command active, checking '
                               'for finished run remotely')
            if self.is_finished(anyfile=anyfile, verbose=False):
                rfile = os.path.join(self.remote_directory, self.resultfile)
                self._logger.debug(f'touching file {rfile} to prevent rerun')
                self.url.cmd(f'touch {rfile}')
                return {'status': 'finished_remotely'}
        self._logger.debug('beginning run prep')
        command = self.call()
        asynchronous = False
        if self.run_options['asynchronous'] and self.url.local:
            asynchronous = True  # command += ' &'
        self._logger.debug(f'running calc with command "{command}" and '
                           f'asynchronous: {asynchronous}')
        self.url.cmd(command, asynchronous=asynchronous)
        return {'status': 'submitted'}

    def post_processing(self, files, status):
        """Fetch the results for finished runs if async. All runs if not."""
        if self.protocol == 'aiida':
            return self.remote_function._result
        if self.run_options['asynchronous']:
            if status == 'finished_locally':
                if self.verbose:
                    print('Data are retrieved from local directory')
                return self.fetch_result(remotely=False)
            elif status == 'finished_remotely':
                return self.fetch_result(remotely=True)
        else:
            return self.fetch_result()


class RemoteDataset(Dataset):
    """
    Defines a set of remote runs, to be executed from a base script and to
    a provided url. This class is associated to a set of remote submissions,
    which may contain multiple calculations. All those calculations are
    expressed to a single url, with a single base script, but with a collection
    of multiple remote runners that may provide different arguments.

    Args:
        label (str): man label of the dataset.
        run_dir (str): local directory of preparation of the data.
        database_file (str): name of the database file to keep track of the
            submitted runs.
        force (str): force the execution of the dataset regardless of the
            database status.
        **kwargs: global arguments of the appended remote runners.
    """
    def __init__(self, label='RemoteDataset', run_dir='/tmp',
                 database_file='database.yaml', force=False,
                 **kwargs):
        Dataset.__init__(self, label=label, run_dir=run_dir, force=force,
                         **kwargs)
        self._logger = logging.getLogger(__name__ + '.RemoteDataset')
        self._logger.debug(f'creating database at {database_file}')

        self.database = RunsDatabase(database_file)
        self._logger.debug('looking for protocol in args')
        self.protocol = kwargs.get('protocol', None)
        self._logger.debug(f'{self.protocol}')

        self._logger.debug('looking for verbose in args')
        self.verbose = kwargs.get('verbose', True)
        self._logger.debug(f'{self.verbose}')

        self.url = _ensure_url(kwargs.get('url', None), self.verbose)
        # self.database_file = database_file
        # self.database = self._construct_database(self.database_file)

    def append_run(self, id, remote_runner=None, **kwargs):
        """Add a remote run into the dataset.

        Append to the list of runs to be performed the corresponding runner and
           the arguments which are associated to it.

        Args:
          id (dict): the id of the run, useful to identify the run in the
             dataset. It has to be a dictionary as it may contain
             different keyword. For example a run might be classified as
             ``id = {'hgrid':0.35, 'crmult': 5}``.
          remote_runner (RemoteRunner): a instance of a remote runner that will
              be employed.
          **kwargs: arguments required for the creation of the corresponding
              remote runner. If remote_runner is provided, these arguments will
              be They will be combined with the global arguments.

        Raises:
          ValueError: if the provided id is identical to another previously
             appended run.
        """
        self._logger.info(f'appending run at id {id} with kwargs '
                          f'{format_iterable(kwargs)}')
        from os.path import join
        # first fill the internal database
        # 'inp' causes problems with aiida, must translate to 'input'
        if 'inp' in kwargs.get('arguments', {}) and self.protocol == 'aiida':
            vals = kwargs['arguments'].pop('inp')
            kwargs['arguments']['input'] = vals
        Dataset.append_run(self, id, Runner(), **kwargs)
        # then create the actual remote runner to be included
        self._logger.debug('creating runner for this run')
        inp = self.runs[-1]
        local_dir = inp.get('local_dir')
        if local_dir is not None:
            basedir = join(self.get_global_option('run_dir'), local_dir)
        else:
            basedir = self.get_global_option('run_dir')
        inp['local_dir'] = basedir
        # for some reason a prior RemoteRunner.run() can add it's own output
        # file here. Force it to be empty.
        # inp['output_files'] = []
        name = self.names[-1]
        # the arguments have to be substituted before the run call
        if remote_runner is not None:
            self._logger.debug('pre-specified runner given in args')
            remote_script = remote_runner
            remote_script.name = name
        else:
            remote_script = RemoteRunner(name=name, **inp)
        self._logger.info(f'added runner {remote_script} with name {name}')
        self.calculators[-1]['calc'] = remote_script

    def pre_processing(self):
        """
        Setup datasets prior to run. Gather and send data to the runners

        For each appended runner: Register them within the database,
        collect the files to be sent then send the files to the run directory
        """
        self._logger.info('preprocessing datasets')
        from warnings import warn

        def get_info_from_runner(irun, info):
            run_inp = self.runs[irun]
            val = run_inp.get(info, remote_runner._global_options.get(info))
            return val

        def get_local_run_info(irun, info):
            return self.run_options.get(info, get_info_from_runner(irun, info))

        # gather all the data to be sent
        files_to_send = {}
        selection = []
        force = self.run_options['force']
        self._logger.debug(f'force is set to {force}, commencing itemisation')
        for irun, (name, calc) in enumerate(zip(self.names, self.calculators)):
            # Check the database.
            do_it = True
            if not force:
                if self.database.exists(name):  # self._check_database(name):
                    run_dir = self.get_global_option('run_dir')
                    warnmsg = f'({name}, {run_dir}) already submitted'
                    self._logger.warning(warnmsg)
                    warn(warnmsg, UserWarning)
                    do_it = False
            if do_it:
                selection.append(irun)
            else:
                continue
            remote_runner = calc['calc']
            remote_dir = get_info_from_runner(irun, 'remote_dir')
            local_dir = get_info_from_runner(irun, 'local_dir')
            self.url.ensure_dir(remote_dir)
            self.url.ensure_dir(local_dir)
            if remote_dir not in files_to_send:
                files_to_send[remote_dir] = []
            files_to_send[remote_dir].extend(remote_runner.setup(
                dest=remote_dir, src=local_dir))
        # then send the data and mark them as sent.
        self._logger.info(f'files to send before run: '
                          f'{format_iterable(files_to_send)} '
                          'sending with a bulk rsync')
        for remote, files in files_to_send.items():
            self.url.rsync(files, local_dir, remote)
        for irun, calc in enumerate(self.calculators):
            self.calculators[irun]['calc'].files_sent(True)
        self.selection = selection
        return {}

    def process_run(self):
        """Run the dataset, by performing explicit run of each of the item of
        the runs_list.
        """
        self._logger.info('running and registering calcs')
        self._run_the_calculations(selection=self.selection)
        # Also, update the database informing that
        # the data should run by now.
        for irun, name in enumerate(self.names):
            if irun not in self.selection:
                continue
            self.database.register(name)  # self._register_database(name)
        return {}

    def is_finished(self, anyfile=True, verbose=False, timeout=-1):
        """Returns all() of is_finished methods of each runner present

        Args:
            anyfile (bool):
                Checks for file recency if False
            verbose (bool):
                Will not print checking status if False
            timeout (int):
                Number of times each is_finished call can fail before raising
                an error. -1 to disable (Default)

        Returns:
            bool: True if all runs have completed
        """
        self._logger.info('checking for finished runs')
        fin = True
        for irun, calc in enumerate(self.calculators):
            if irun in self.selection:
                fin = fin and \
                      calc['calc'].is_finished(
                          anyfile=anyfile,
                          verbose=verbose,
                          timeout=timeout)

        # fin = [run['calc'].is_finished(anyfile=anyfile, verbose=verbose) for
        #        run in self.calculators]

        return fin


class RunsDatabase():
    """Contains the list of runs which have been submitted.

    Args:
        database_file (str): name of the file used to store the data
    """
    def __init__(self, database_file):
        self._logger = logging.getLogger(__name__ + '.RunsDatabase')
        self.filename = self._ensure_name(database_file)
        self.database = self._construct_database(self.filename)

    def _ensure_name(self, name):
        """forces the database file to be a .yaml file"""
        # not a yaml file
        if not name.endswith('.yaml'):
            self._logger.debug(f'{name} does not end with .yaml: Translating')
            if '.' in name:
                self._logger.debug('replacing old extension')
                yamlfile = '.'.join(name.split('.')[:-1]) + '.yaml'
            else:
                yamlfile = name + '.yaml'
        else:
            yamlfile = name
        self._logger.info(f'ensuring name of database file {name}: '
                          f'{yamlfile}')

        return yamlfile

    def _construct_database(self, name):
        self._logger.info(f'constructing database at {name}')
        from yaml import load, Loader
        try:
            with open(name, "r") as ifile:
                return load(ifile, Loader=Loader)
        except FileNotFoundError:
            return []

    def _write_database(self):
        from yaml import dump
        # Update the file
        with open(self.filename, "w") as ofile:
            dump(self.database, ofile)

    def exists(self, name):
        """Checks if a name exists in the database."""
        self._logger.info(f'checking if {name} exists in database')
        return name in self.database

    def register(self, name):
        """Include a name in the database."""
        self._logger.info(f'adding {name} to database')
        if not self.exists(name):
            self._logger.debug('name does not exist, appending')
            self.database.append(name)
            self._write_database()
        else:
            self._logger.debug('name exists already, skipping')

    def clean(self):
        """Remove the database information."""
        self._logger.debug('wiping database...')
        for name in list(self.database):
            self._logger.debug(f'\tremoving {name}')
            self._remove_database_entry(name)

    def _remove_database_entry(self, name):
        if name in self.database:
            self.database.remove(name)
        self._write_database()


def computer_runner(func, submission_script, url=None, **kwargs):
    """Create a runner based on a computer information.

    A function is transformed into a remote runner from the specification
    of a computer.

    Args:
        func (func): function to be transformed
        submission_script (PyBigDFT.BigDFT.RemoteRunnerUtils.CallableAttrDict):
            specification dictionary of the computer. Should contain the
            attribute ``submitter``.
        url (BigDFT.URL.URL):
            The url of the computer
        **kwargs: arguments of the RemoteRunner

    Returns:
        RemoteRunner: a new remote runner ready for the computer.
    """
    from BigDFT.URL import URL
    return RemoteRunner(func, submitter=submission_script.submitter,
                        url=url if url is not None else URL(),
                        script=submission_script(), **kwargs)
