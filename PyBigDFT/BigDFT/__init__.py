import logging
import os

import BigDFT.LogUtils as LogUtils

# determine log path
manualpath = False
if 'BigDFT_logpath' in os.environ.keys():
    logpath = os.path.abspath(os.environ['BigDFT_logpath'])
    manualpath = True
else:
    filepath = os.path.abspath(__file__)
    pybigdftpath = os.path.abspath(os.path.join(filepath, '../..'))

    logpath = os.path.abspath(pybigdftpath)

logpath = os.path.join(logpath, 'BigDFT_log.yaml')
if manualpath:
    print(f'accessing logfile at {logpath}')

# create logger, file handler and formatter
module_logger = logging.getLogger(__name__)

file_handler = logging.FileHandler(logpath)
formatter = logging.Formatter('%(asctime)s - '
                              '%(levelname)s - '
                              '%(name)s.%(funcName)s: '
                              '%(message)s',
                              datefmt='%Y-%m-%d %H-%M-%S')

# connect it all together and initialise
file_handler.setFormatter(formatter)
module_logger.addHandler(file_handler)

# log level setting
# module_logger.setLevel(LogUtils.levels['INFO'])  # keep log empty

# extract log level from os.environ dict, if possible
loglvl = None
loglvl_from_env = False
if 'BigDFT_loglvl' in os.environ.keys():
    loglvl = LogUtils._sanitise_loglvl(os.environ['BigDFT_loglvl'])
    loglvl_from_env = True
else:
    loglvl = 'WARNING'

module_logger.setLevel(LogUtils.levels[loglvl])
module_logger.info('#'*24 + ' new module call, creating logger ' + '#'*24)

if loglvl_from_env:
    module_logger.info(f'setting global log level to env variable '
                       f'{LogUtils.levels[loglvl]} ({loglvl})')
    print('reading log level from BigDFT_loglvl environment variable: '
          f'set to {LogUtils.levels[loglvl]} ({loglvl})')
else:
    module_logger.info(f'setting global log level to default '
                       f'{LogUtils.levels[loglvl]} ({loglvl})')
