"""
The URL handler exists to assist connection to a remote system.

As it also wraps file transfer and command execution systems, even non-remote
runs should rely on the use of a URL class. For developers:
This allows RemoteRunners to focus on the file and job handling aspects

The following examples should give you an idea of basic usage of the URL
module. Lets assume we have a remote server we want to run on, with IP
address ``192.168.0.0``, and we have access via user ``user``


URL can be initialised empty then updated with connection details, or
produced in a number of ways:

>>> u = URL()
>>> print(u.local)
>>> True
>>> u.user = 'user'
>>> u.host = '192.168.0.0'
>>> print(u.local)
>>> False

Initialised with args from the start:

>>> u = URL(user='user', host='192.168.0.0')
>>> print(u.local)
>>> False

Or built from a "legacy" style string:

>>> u = URL.from_string('user@192.168.0.0')
>>> print(u.local)
>>> False

A URL instance can also connect to an existing SSH tunnel, simply specify
your local endpoint. (URL uses the presence of a port to detect tunnels).

>>> u = URL(user='user', host='127.0.0.10', port=1000)
>>> print(u.local)
>>> False
>>> print(u.is_tunnel)
>>> True

For support creating a tunnel connection, URL has the ability to assist
by providing the commands needed. Though you must provide the address of
the tunnel target for this

>>> u = URL.from_tunnel(user='user', host='192.168.0.0')

You will then be given two commands to enter on the remote:

- command 1 sets up the tunnel
- command 2 tests the tunnel

Once this is done, confirm to the tunnel constructor that the tunnel is
active with "y" and URL will connect.

See URL.from_tunnel() for more details.

There is also the functionality to test the connection with the remote:

``URL.test_connection()``

This function, when called, will attempt to create folders on both local
and remote systems, then test file transfer functionality between them.

Further details of attributes and methods are below:
"""

import subprocess
import os

from warnings import warn

import logging
from BigDFT.LogUtils import format_iterable


# create and link logger
module_logger = logging.getLogger(__name__)


def _userhost(user, host, sep='@'):
    """skip"""
    if user is None:
        return host
    else:
        return sep.join((user, host))


class URL:
    """
    Container to store the connection info for a Remote run

    Arguments:
        user (str):
            username for the remote system
        host (str):
            host address of the remote system
        port (int, str):
            port to connect to for ssh tunnels
    """

    _tunnel_port_range = (1000, 10000)

    _rsync_cp_fallback = 'cp fallback'
    _rsync_not_available_msg = 'no file transfer available'

    def __init__(self,
                 user: str = None,
                 host: str = None,
                 port: int = None,
                 rsync: str = None,
                 verbose: bool = False):

        self._logger = logging.getLogger(__name__ + '.URL')

        self._logger.info('creating connection with params: '
                          f'user: {user}, host: {host}, port: {port}')

        # connection details
        self._conn = {'user': user,
                      'host': host,
                      'port': port}

        self._validation = {}

        if rsync is None:
            rsync = 'auv'
        self._rsync_flags = Flags(rsync)

        self._verbose = verbose

        self.utils = URLUtils(self)

    def __repr__(self):
        if self.local:
            return 'URL()'
        if self.is_tunnel:
            return f'URL(host={self.host}, port={self.port})'
        return f'URL(user={self.user}, host={self.host})'

    @classmethod
    def from_string(cls,
                    string: str,
                    rsync: str = None,
                    verbose: bool = False):
        """Create a connection from a `user@hostname` string

        Arguments:
            string (str):
                a `user@hostname` string to create a connection from
            rsync (str, optional):
                initial flags for rsync
            verbose (bool, optional):
                sets verbosity for connection actions

        Returns:
            URL
        """

        module_logger.debug(f'creating URL() from {string}')

        if string is None or str == '':
            module_logger.warning('attempting to create a URL instance '
                                  'from None or empty string. '
                                  'Creating localhost URL')
            return cls(verbose=verbose, rsync=rsync)

        if '@' not in string:
            module_logger.error(f'could not split string {string}')
            raise ValueError('please provide a user@host string')

        userhost = string.split('@')
        user = userhost[0] if len(userhost) == 2 else None
        host = userhost[-1]  # in case the user is not specified

        module_logger.debug(f'string split into {user} and {host}')

        return cls(user=user, host=host, verbose=verbose, rsync=rsync)

    @classmethod
    def from_tunnel(cls,
                    host: str,
                    user: str = None,
                    port: int = None,
                    endpoint: str = None,
                    rsync: str = None,
                    background: bool = False):
        """
        Create a URL instance based on an ssh tunnel, giving the needed command

        Guides the user through tunnel creation, creating the command
        corresponding to the desired connection details.

        .. note::

            It is advised to research and set up the tunnel yourself for your
            environment, as this method does not adapt per use case. Therefore
            the provided commands may set up an incorrect (or nonfunctional)
            tunnel.

        Arguments:
            user (str):
                username for remote host
            host (str):
                hostname to connect to
            port (int, optional):
                local port to connect to. Defaults to ``1800``
            endpoint (str, optional):
                local tunnel endpoint. Defaults to ``127.0.0.10``
            rsync (str, optional):
                rsync flags to be given to URL, if needed
            background (bool, optional):
                place the process in the background

                .. warning::

                    The process should be killed when not needed if placed
                    in the background

        Returns:
            URL

        When creating a tunnel connection you must specify the address of the
        remote server.

        For example, to connect to our ``192.168.0.0`` server, the connection
        details for a standard ``ssh`` are required variables. You can
        optionally specify the tunnel *endpoint*: The "local" IP address and
        port of the tunnel.

        When creating a tunnel, you will be given two commands:

        - tunnel creation command
        - tunnel test command

        The tunnel creation command will look similar to this:

        ``ssh user@192.168.0.0 -L 1000:127.0.0.10:192.168.0.0:22 -N``

        Whereas the test command will be a simple ssh "ls" command to ensure
        that the tunnel is operational and allows info transfer.

        ``ssh -p 1000 127.0.0.10 "ls"``

        Assuming everything works, enter ``y`` to continue.
        """

        # LG: user can be omitted (useful if the info is in ssh config)
        # if user is None:
        #     raise ValueError('Please provide a username')

        if host is None:
            raise ValueError('Please provide a hostname')

        if port is None:
            # port = random.randint(*URL._tunnel_port_range)
            port = 1800

        if endpoint is None:
            endpoint = '127.0.0.10'

        bg = 'f' if background else ''

        userhost = _userhost(user, host)

        tunnelstring = f'ssh {userhost} -L ' \
                       f'{endpoint}:{port}:{host}:22 -{bg}N'

        module_logger.debug('attempting tunnel creation using command '
                            f'"{tunnelstring}"')

        inputmsg = f"""Use the following command to create the tunnel:
        \n{tunnelstring}\n
        \nThen attempt the following command (in a separate terminal if needed)
        \nssh -p {port} {endpoint} "ls"\n
        \ncontinue (y/n)? """

        cont = ''
        timeout = 10
        count = 0
        while cont.lower() != 'y':
            count += 1
            if count > timeout:
                module_logger.error('tunnel creation exited after timeout')
                raise Exception('tunnel creation failed')
            cont = input(inputmsg)
            module_logger.debug(f'user entered: "{repr(cont)}"')
            if cont == 'n':
                module_logger.debug('user exited tunnel creation')
                raise Exception('tunnel creation exited')

        module_logger.debug('attempting to connect to tunnel endpoint')

        return cls(user, endpoint, port, rsync)

    def userhost(self, sep='@'):
        return _userhost(self.user, self.host, sep)

    @property
    def user(self):
        """Returns the specified username"""
        return self._conn['user']

    @user.setter
    def user(self, user: str):
        """Sets the username attribute"""
        self._logger.debug(f'setting username to {user}')
        self._conn['user'] = user

    @property
    def host(self):
        """Returns the specified hostname"""
        return self._conn['host']

    @host.setter
    def host(self, host: str):
        """Sets the hostname attribute"""
        self._logger.debug(f'setting hostname to {host}')
        self._conn['host'] = host

    @property
    def port(self):
        """Returns the specified ssh tunnel port"""
        return self._conn['port']

    @port.setter
    def port(self, port: int):
        """Sets the port attribute"""
        self._logger.debug(f'setting port to {port}')
        self._conn['port'] = port

    @property
    def verbose(self):
        """Returns the verbosity setting"""
        return self._verbose

    @verbose.setter
    def verbose(self, verbose: bool):
        """Sets the verbosity"""
        self._logger.debug(f'setting verbosity to {verbose}')
        self._verbose = verbose is True

    @property
    def local(self):
        """Returns True if this connection is local only"""
        # use the lack of a hostname to determine
        # as user and port can be either
        return self._conn['host'] is None

    @property
    def is_tunnel(self):
        """Returns True if this connection uses an SSH tunnel"""
        return self._conn['port'] is not None

    @property
    def rsync_flags(self):
        """Returns the flags used during rsync process"""
        return self._rsync_flags.flags

    @rsync_flags.setter
    def rsync_flags(self, flags: str):
        """Sets the flags for an rsync process"""
        self._rsync_flags.flags = flags

    @property
    def permissions(self):
        """
        Displays the available permissions for this URL

        Dictates whether files can be created in root, or only in the user
        directories

        .. note::
            This property will test the connection if no permissions can be
            found

        Returns:
            dict:
                permissions
        """
        perms = ['local_permissions', 'remote_permissions']
        self._logger.debug(f'searching for permissions in {perms}')
        if not any([p in self._validation.keys() for p in perms]):
            self._logger.warning(f'no permissions found in validation dict: '
                                 f'{format_iterable(self._validation)}')
            self.test_connection(verbose=False)

        ret = {k: v for k, v in self._validation.items() if k in perms}
        return ret

    @property
    def ssh(self) -> str:
        """ssh command to be used for this connection"""
        self._logger.debug('generating ssh command')
        ret = ['ssh ']
        if self.is_tunnel:
            self._logger.debug('tunnel connection, adding "-p port"')
            ret.append(f'-p {self.port} ')
        elif not self.local:
            self._logger.debug('remote connection, adding host')
            ret.append(f'{self.userhost()}')
        else:
            self._logger.warning('local connection, returning empty ssh')
            return ''
        ssh = ''.join(ret)
        self._logger.debug(f'ssh command: {ssh}')
        return ssh

    def cmd(self,
            cmd: str,
            asynchronous: bool = False,
            local: bool = None,
            verbose: bool = None,
            suppress_warn: bool = False) -> str:
        """
        Execute a command

        Arguments:
            cmd (str):
                the command to be executed
            asynchronous (bool, optional):
                do not wait for stdout if async
            local (bool, optional):
                force local or remote execution
            verbose (bool, optional):
                verbosity override for this execution
            suppress_warn (bool, optional):
                suppress warnings

        Returns:
            str:
                stdout if not async, None otherwise
        """
        self._logger.debug(f'executing command {repr(cmd)} '
                           f'with forced local verbosity {verbose} '
                           f'in working directory {os.getcwd()}')
        if verbose is None:
            verbose = self.verbose
            self._logger.debug(f'updating verbose to URL.verbose ({verbose})')

        # ensure the command is being executed where we need
        # disable if local only, this will result in a "command", which doesn't
        # run
        if not self.local and local is not None:
            if not local and 'rsync' not in cmd:
                self._logger.debug('forced remote connection and not rsync, '
                                   'generating and adding ssh')
                cmd = f'{self.ssh} "{cmd}"'

        elif not self.local and 'rsync' not in cmd:
            self._logger.debug('implicit remote connection and not rsync, '
                               'generating and adding ssh')
            cmd = f'{self.ssh} "{cmd}"'
        # TODO(lbeal) add protection against "rm -r *" commands
        if verbose:
            print('executing command: ' + repr(cmd))
        self._logger.debug(f'sending command {repr(cmd)}')
        # {brace expansion} needs '/bin/bash' in some cases (seen with cp)
        # https://stackoverflow.com/a/22660171
        sub = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=True,
                               text=True,
                               executable='/bin/bash')
        self._logger.debug('done')
        if not asynchronous:
            ret, err = sub.communicate()
            ret_log = format_iterable(ret.split('\n'))
            self._logger.debug(f'returned string: '
                               f'{ret_log}')
            if err is not None and err != '':
                self._logger.warning(f'command: {repr(cmd)}')
                self._logger.warning(f'returned error: {err}')
                if not suppress_warn:
                    warn(str(err), UserWarning)
            if verbose:
                print('return: ' + ret)
            if hasattr(ret, 'decode'):
                ret = ret.decode('utf-8')
            return str(ret)
        else:
            return None

    def ls(self,
           target: str = './',
           local: bool = False) -> list:
        """
        Perform `ls` command on target

        Arguments:
            target (str):
                target entity (folder or file)
            local (bool):
                force local or remote search

        Returns:
            list:
                newline split result
        """
        self._logger.debug(f'performing ls on {target}')
        ls_ret = self.cmd(f'ls {target}', local=local).split('\n')
        self._logger.debug(f'ls result for {target}: {ls_ret}')

        return [f for f in ls_ret if f != '']

    def rsync(self,
              files: list,
              origin: str,
              target: str = None,
              push: bool = True,
              dry_run: bool = False):
        """
        Create rsync "link" between origin and target, pull or push files

        Arguments:
            files (list):
                list of filenames to transfer
            origin (str):
                origin folder
            target (str):
                target folder
            push (bool):
                files are to be sent (or received)
            dry_run (bool):
                don't send the command if True
        Returns:
            str:
                the rsync command stdout or the command if dry_run
        """
        self._logger.debug(f'performing rsync from {origin} to {target} '
                           f'for {len(files)} files')
        # can work without a target, but use same folder name as origin
        if target is None:
            self._logger.warning(f'rsync target is None, setting target to '
                                 f'origin ({origin}')
            target = origin
        # check if we're using 'cp' as a fallback (rsync not available)
        cp_fallback = False
        if hasattr(self, '_validation'):
            rsync = self._validation.get('rsync', 'rsync')
            self._logger.debug(f'validation currently: '
                               f'{format_iterable(self._validation)}')
            if rsync == self._rsync_cp_fallback:
                self._logger.warning('rsync not available, but cp_fallback is '
                                     'set. Replacing rsync calls with cp')
                cp_fallback = True

        # initial setup with command and flags
        cmd = ['rsync', self._rsync_flags.flags]
        if cp_fallback:  # override base command if cp_fallback
            cmd = ['cp']
        # if we're tunnelling, the port has to be specified here
        # also set up target folder connection address if needed
        if self.is_tunnel:
            cmd.append(f"'-e ssh -p {self.port}'")

            target = f'{self.host}:{target}'
        elif not self.local:
            target = f'{self.userhost()}:{target}'

        self._logger.debug(f'target updated to {target}')

        srt = origin if push else target
        end = target if push else origin
        self._logger.debug(f'as push is {push}, moving files from '
                           f'{srt} to {end}')
        if self.verbose:
            print('sending', files)
            print(f'from {srt} to {end}')
        # wrap files into a {container} where possible
        # for this syntax to be valid we must be transferring >1 files
        if len(files) <= 1:
            for file in files:
                cmd.append(os.path.join(srt, file))
        else:
            temp = [os.path.join(srt, '{')]
            for file in files:
                temp.append(file + ',')
            cmd.append(''.join(temp)[:-1] + '}')

        cmd.append(end)
        cmd = ' '.join(cmd)
        self._logger.debug(f'rsync command produced: {cmd}')
        if dry_run:
            return cmd
        return self.cmd(cmd)

    def ensure_dir(self,
                   dir: str,
                   force_local: bool = False):
        """
        Ensure a directory exists by attempting to create it

        Arguments:
            dir (str):
                directory to create
            force_local (bool):
                force a local run

        Warning: Duplicate of futile.Utils.ensure_dir
        """
        self._logger.debug(f'ensuring presence of dir {dir}')
        if self.local or force_local:
            self._logger.debug('running locally, use os module')
            try:
                os.mkdir(dir)
                self._logger.debug(f'local creation of {dir} succeeded')
            except FileExistsError:
                self._logger.debug(f'ensure dir of {dir} (already exists)')
                pass
        else:
            self._logger.debug('running on remote, using cmd')
            self.cmd(f'mkdir -p {dir}')

    def test_connection(self,
                        verbose: bool = None) -> bool:
        """
        Check the connection by testing that we can create directories
        on local, remote and send/receive files

        check file creation permissions for remote and local
        then test that we can create files and rsync them

        Arguments:
            verbose (bool):
                verbosity for this test

        Returns:
            bool:
                True if test was successful
        """
        self._validation = {}
        # override verbosity for these tests
        # (they can produce a lot of output)
        old_verbose = self.verbose
        local_verbose = True
        if verbose is None:
            # minimal verbosity, but not off
            self.verbose = False
        elif verbose:
            # fully on
            self.verbose = True
        elif not verbose:
            # fully off
            self.verbose = False
            local_verbose = False

        self._logger.debug(f'testing connection with verbosity '
                           f'{local_verbose}')

        # test local folder creation
        if local_verbose:
            print('\n'+'#'*24)
            print('checking local permissions')
        local_fld = self._file_create_validation(root='/',
                                                 local=True,
                                                 verbose=local_verbose)
        self._logger.debug(f'folder returned from local: {local_fld}')
        if local_fld == '':
            self._logger.warning('system could not create local folder')
            return False
        # test remote folder creation
        if local_verbose:
            print('#'*24)
            print('checking remote permissions')
        remote_fld = self._file_create_validation(root='/',
                                                  local=False,
                                                  verbose=local_verbose)
        self._logger.debug(f'folder returned from remote: {remote_fld}')
        if remote_fld == '':
            self._logger.warning('system could not create remote folder')
            return False

        # test rsync for the created folder
        self._logger.debug('testing rsync file sending')
        success_l = self._file_transfer_validation(local_fld,
                                                   remote_fld,
                                                   True,
                                                   local_verbose)

        self._logger.debug('testing rsync file receiving')
        success_r = self._file_transfer_validation(local_fld,
                                                   remote_fld,
                                                   False,
                                                   local_verbose)

        if local_verbose:
            print('\n' + '#'*24)
            print('cleaning up...')
            print(f'\tremoving folders: '
                  f'\n\tremote: {remote_fld}, '
                  f'\n\tlocal: {local_fld}')
        self._logger.debug('cleaning up: removing folders:')
        self._logger.debug(f'\tremote: {remote_fld}')
        self._logger.debug(f'\tlocal: {local_fld}')
        self.cmd(f'rm -r {remote_fld}')
        self.cmd(f'rm -r {local_fld}', local=True)

        self._logger.debug('setting verbose back to old value')
        self.verbose = old_verbose

        self._logger.info('connection test completed, validation is now: '
                          f'{format_iterable(self._validation)}')

        if success_l and success_r:
            return True
        return False

    def _file_create_validation(self, root, local, verbose):
        if not local and self.local:
            self._logger.warning('system is local only, forcing local for '
                                 'remote connection test file creation')
            # can't test remote permissions if there's no remote
            local = True
        # permission that we're updating
        permission = 'local_permissions' if local else 'remote_permissions'
        # directory access
        prior = self.ls(root, local=local)
        # generate a folder name that doesn't already exist
        test_folder = 'URL_connection_test'
        i = 0
        timeout = 10
        while test_folder in prior:
            if i > timeout:
                raise ValueError('could not create folder, '
                                 f'try cleaning out {root}')
            i += 1
            self._logger.warning(f"can't create test folder {test_folder}: "
                                 "already exists")
            if verbose:
                print(f'{test_folder} already exists ({i}/{timeout})')
            test_folder = 'URL_connection_test_' + str(i)
        abspath_folder = os.path.join(root, test_folder)
        if verbose:
            print(f'creating test folder {abspath_folder}')
        self._logger.debug(f'test creating test folder {abspath_folder}')

        # if we're not running in root, need folder break
        # this / will mess with checks, however, so need a new variable
        self.cmd(f'mkdir -p {abspath_folder}', local=local,
                 suppress_warn=True)

        if verbose:
            print(f'looking for {test_folder} in {root}')
        self._logger.debug(f'checking for folder presence in {root}')
        folder_created = test_folder in self.ls(root, local=local)

        # if folder not created in root, can we redirect to user folders?
        if not folder_created:
            if root == '/':
                if verbose:
                    print('\tpermission denied, trying again in user folders')
                self._logger.debug("permission denied but we're in root, "
                                   "so move to home directory and try again")
                if local:
                    homedir = os.environ['HOME']
                else:
                    homedir = self.cmd('echo $HOME').rstrip('\n')
                return self._file_create_validation(f'{homedir}',
                                                    local=local,
                                                    verbose=verbose)
            else:
                if self.verbose:
                    print('\tfailure, returning empty string')
                self._logger.warning('folder permission denied when testing '
                                     f'{permission}')
                self._validation[permission] = ''
                return ''
        if self.verbose:
            print(f'success! (local: {local}) returning {abspath_folder}')
        self._logger.debug(f'successful test for {permission}')
        self._validation[permission] = 'root' if root == '/' else 'user'
        return f'{abspath_folder}'

    def _file_transfer_validation(self,
                                  local_fld,
                                  remote_fld,
                                  push,
                                  local_verbose):

        self._logger.debug(f'file transfer validation with local {local_fld}, '
                           f'remote {remote_fld}. push = {push}')

        dirn = 'push' if push else 'pull'
        if local_verbose:
            print('\n' + '#'*24)
            print(f'checking that we can {dirn} files')

        test_file = f'testfile_{dirn}'
        start_fld = local_fld if push else remote_fld
        dest_fld = remote_fld if push else local_fld

        self._logger.debug(f'{test_file} from {start_fld} to {dest_fld}')

        self.cmd(f'touch {start_fld}/{test_file}', local=push)
        self.rsync([test_file], local_fld, remote_fld, push=push)

        success = test_file in self.ls(f'{dest_fld}', local=not push)

        if success:
            # repeated tests will use the fallback if enabled. So make sure
            # it does not get set back to rsync here if that is the case
            # use a get on the validation dict, defaulting back to 'rsync'
            self._validation['rsync'] = self._validation.get('rsync', 'rsync')
        elif self.local:
            # if we're local only, can use 'cp' as a fallback
            self._logger.warning('rysnc test failed, testing cp as a fallback')
            self.cmd(f'cp {start_fld}/{test_file} {dest_fld}', local=True)

            success = test_file in self.ls(f'{dest_fld}', local=True)

            if success:
                self._logger.warning('cp succeeded. Using for now (THIS '
                                     'SHOULD BE TREATED AS AN ISSUE)')
                self._validation['rsync'] = self._rsync_cp_fallback
            else:
                self._logger.warning('cp also failed, check permissions and '
                                     'commands passed')
        if not success:
            self._validation['rsync'] = self._rsync_not_available_msg

        return success


class URLUtils:
    """
    Extra functions to go with the URL class, called via URL.utils

    As it requires a parent `URL` to function, and is instantiated with a
    `URL`, there is little to no purpose to using this class exclusively

    Arguments:
        parent (URL):
            parent class to provide utils to
    """
    def __init__(self, parent: URL):

        self._logger = logging.getLogger(__name__ + '.URLUtils')
        self._logger.info(f'creating a utils extension to parent: {parent}')

        self._parent = parent

    def search_folder(self, files: list, folder: str, local: bool = False):
        """
        Search `folder` for `files`, returning a boolean presence dict

        Arguments:
            files (list):
                list of filenames to check for. Optionally, a string for a
                single file
            folder (str):
                folder to scan
            local (bool):
                perform the scan locally (or remotely)
        Returns (dict):
            {file: present} dictionary
        """
        fpath = os.path.abspath(folder)

        self._logger.debug(f'scanning folder {folder}. '
                           f'Convert to abspath {fpath}')
        self._logger.debug(f'searching for files: {format_iterable(files)}')

        ls_return = self._parent.cmd(f'ls {fpath}', local=local)

        scan = [os.path.basename(f) for f in
                ls_return.split('\n')]

        self._logger.debug(f'scan sees: {format_iterable(scan)}')

        if isinstance(files, str):
            self._logger.info('files is a string, running in singular mode')

            ret = {files: os.path.basename(files) in scan}

        else:
            ret = {}
            for file in files:
                fname = os.path.basename(file)
                ret[file] = fname in scan

        return ret

    def touch(self, file: str, local: bool = False):
        """
        perform unix `touch`, creating or updating `file`

        Arguments:
            file (str):
                filename or path to file
            local (bool):
                force local (or remote) execution
        """
        self._logger.debug(f'utils touch on file {file}')
        fname = os.path.abspath(file)
        self._parent.cmd(f'touch {fname}', local=local)

    def mkdir(self, file, local=False):
        """
        perform unix `mkdir -p`, creating a folder structure

        Arguments:
            file (str):
                name or path to folder
            local (bool):
                force local (or remote) execution
        """
        self._logger.debug(f'utils mkdir on path {file}')
        fname = os.path.abspath(file)
        print(f'making dir {fname}')
        self._parent.cmd(f'mkdir -p {fname}', local=local)

    def ls(self, file: str, as_list: bool = True, local: bool = False):
        """
        perform unix `mkdir -p`, creating a folder structure

        Arguments:
            file (str):
                name or path to folder
            as_list (bool):
                convert to a list format
            local (bool):
                force local (or remote) execution
        """
        self._logger.debug(f'utils ls on path {file}')
        fname = os.path.abspath(file)

        ret = self._parent.cmd(f'ls {fname}', local=local)

        if as_list:
            ret = [f for f in ret.split('\n') if f != '']
        return ret


class Flags:
    """
    Basic but flexible handler for terminal command flags

    Allows for inplace modification:

    >>> f = Flags('abcdd')
    >>> f -= 'd'
    >>> f.flags
    >>> '-abcd'

    Arguments:
        initial_flags (str):
            initial base flags to be used and modified if needed
        prefix (optional, str):
            the non-argument prefix for these flags (`-`, `--`, etc.)
    """

    _logger = logging.getLogger(__name__ + '.Flags')

    def __init__(self, initial_flags: str = '', prefix: str = None):

        self._logger.debug(f'creating Flags with initial flags '
                           f'{initial_flags} and prefix {prefix}')

        self._prefix = ''
        if prefix is None:
            if '-' in initial_flags:
                self.prefix = '-' * initial_flags.count('-')
            else:
                self.prefix = '-'
            self._logger.debug(f'prefix set to {self.prefix}')
        else:
            self.prefix = prefix

        self.flags = initial_flags

    def __repr__(self):
        return f'Flags({self.flags})'

    def __add__(self, other):
        self._flags = self._flags + other.strip('-')
        self._logger.debug(f'adding {other} to flags. '
                           f'Flags are now {self.flags}')

    def __iadd__(self, other):
        self.__add__(other)
        self._logger.debug(f'adding {other} to flags inplace. '
                           f'Flags are now {self.flags}')
        return self

    def __sub__(self, other):
        """Subtract unique flags in other once."""
        for char in ''.join(set(other)):
            self._flags = self._flags.replace(char, '', 1)
        self._logger.debug(f'subtracting {other} from flags. '
                           f'Flags are now {self.flags}')

    def __isub__(self, other):
        self.__sub__(other)
        self._logger.debug(f'subtracting {other} from flags inplace. '
                           f'Flags are now {self.flags}')
        return self

    @property
    def flags(self):
        """Returns the fully qualified flags as a string"""
        if len(self._flags) == 0:
            return ''
        return self.prefix + self._flags

    @flags.setter
    def flags(self, inp):
        """Set the flags to the new input

        Arguments:
            inp (str):
                new flags to use (will overwrite old ones)
        """
        self._logger.debug(f'setting flags to {inp}')
        for char in set(self.prefix):
            inp = inp.strip(char)
        self._logger.debug(f'after cleaning, flags are now: {inp}')
        self._flags = inp

    @property
    def prefix(self):
        """Returns the - prefix used for these flags"""
        return self._prefix

    @prefix.setter
    def prefix(self, prefix):
        """Sets the new - prefix level

        Arguments:
            prefix (str):
                new prefix to precede flags with
        """
        self._prefix = prefix


if __name__ == '__main__':

    tunnel = False
    test = URL(rsync='aux')
    if tunnel:
        test.host = '127.0.0.10'
        test.port = 1800
    else:
        test.host = '192.168.0.17'
    test.user = 'pi'

    print(test)
    test.test_connection()
