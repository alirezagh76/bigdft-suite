"""
Provides utilities for BigDFT logging events
"""

import logging


levels = {'CRITICAL': logging.CRITICAL,
          'ERROR': logging.ERROR,
          'WARNING': logging.WARNING,
          'INFO': logging.INFO,
          'DEBUG': logging.DEBUG}


def _sanitise_loglvl(levelstr):
    """
    Clean the environment variable log level

    $EXPORT BigDFT_loglvl = LEVEL
    to change
    """
    if levelstr.upper() in levels.keys():
        return levelstr.upper()
    else:
        return None


def format_iterable(iterable,
                    exclude=None,
                    indent: int = 1) -> str:
    """
    Recursive formatting of an iterable, producing log-safe string

    Arguments:
        iterable (list, tuple, set, dict):
            iterable to sort
        exclude (list, optional):
            keys to exclude
        indent (int, optional):
            indent level for recursion
    Returns:
        (str):
            newline separated string
    """
    # can hit empty lists (often in recursion)
    # output looks nicer if they're output without formatting
    if len(iterable) == 0:
        return '\n' + '\t'*indent + f'{iterable}'

    if exclude is None:
        exclude = []
    elif isinstance(exclude, str):
        exclude = [exclude]

    # not a true dispatch table, but allows mode compression
    dispatch = {dict: 0,
                list: 1,
                tuple: 1,
                set: 1,
                str: 2,
                }
    mode = dispatch[type(iterable)]

    # parse the iterable
    ret = ''
    if mode == 0:

        ret = []
        for a, v in iterable.items():

            if a not in exclude:
                if type(v) in dispatch.keys():
                    v = format_iterable(iterable=v,
                                        exclude=exclude,
                                        indent=indent+1)
                ret.append('\t'*indent + f'{a}: {v}')

        ret = '\n'.join(ret)

    elif mode == 1:
        exclude.append('')

        ret = []
        for v in iterable:

            if v not in exclude:
                if type(v) in dispatch.keys():
                    v = format_iterable(iterable=v,
                                        exclude=exclude,
                                        indent=indent+1)
                ret.append('\t'*indent + f'{v}')

        ret = '\n'.join(ret)

    elif mode == 2:
        return iterable

    return '\n' + ret
