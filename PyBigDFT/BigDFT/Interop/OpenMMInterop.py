"""
This module contains some wrappers for using OpenMM to perform
various operations on BigDFT molecules.

http://docs.openmm.org/development/api-python/index.html
"""

from BigDFT.Systems import System


def get_available_ff_names():
    import os
    import sys
    names = []
    for path in sys.path:
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    if 'data' in filename and 'openmm' in filename:
                        iname = filename.index('data')+5
                        names.append(filename[iname:])
    return names


class OMMSystem(System):
    """
    Class of OpenMM binder system which enables functionalities
    of openMM on a BigDFT system

    Args:
        system (System): a instance of a system class
        filename (str): name of the PDB file to instantiate the system
    """
    def __init__(self, system=None, filename=None):
        from openmm.app.pdbfile import PDBFile
        from openmm import app
        from BigDFT.IO import read_pdb, write_pdb
        from tempfile import NamedTemporaryFile as tmp
        if filename is not None:
            pdb = PDBFile(open(filename))
            sys = read_pdb(open(filename))
        elif system is not None:
            sys = system
            ofile = tmp('w+')
            write_pdb(system=system, ofile=ofile)
            ofilename = ofile.name
            pdb = PDBFile(open(ofilename))
            ofile.close()
        System.__init__(self, **sys.dict())
        self.pdb = pdb
        self.modeller = app.Modeller(pdb.topology, pdb.positions)

    def set_forcefields(self, *ff_list):
        """
        Define a set of force fields that will be used in the geometry ops.

        Args:
            *ff_list: list of the force fields to be included,
                in priority order.
                Use the :func:py:`get_available_ff_names` to identify the
                available force fields
        """
        from openmm import app
        self.forcefield = app.ForceField(*ff_list)

    @property
    def OMMsystem(self):
        from openmm import app
        if not hasattr(self, '_system'):
            self._system = self.forcefield.createSystem(
                self.modeller.topology, nonbondedMethod=app.NoCutoff,
                constraints=None)
        return self._system

    def set_integrator(self, T=298.15):
        from openmm import unit as u
        import openmm.openmm as mm
        temperature = T * u.kelvin
        self.integrator = mm.LangevinIntegrator(
            temperature, 1 / u.picosecond,  0.0005 * u.picoseconds)

    @property
    def OMMsimulation(self):
        from openmm import app
        if not hasattr(self, '_simulation'):
            self._simulation = app.Simulation(self.modeller.topology,
                                              self.OMMsystem, self.integrator)
            self._simulation.context.setPositions(self.modeller.positions)
        return self._simulation

    def OMMenergy(self, units='kcal/mol'):
        from openmm.openmm import KcalPerKJ
        energy = self.OMMsimulation.context.getState(
             getEnergy=True).getPotentialEnergy()
        return energy._value * KcalPerKJ

    @property
    def OMMposition(self):
        return self.OMMsimulation.context.getState(
            getPositions=True).getPositions()

    def write(self, ofile):
        from openmm import app
        app.PDBFile.writeFile(self.OMMsimulation.topology, self.OMMposition,
                              open(ofile, 'w'))

    def optimize(self, iters):
        self.OMMsimulation.minimizeEnergy(maxIterations=iters)

    @property
    def forcegroups(self):
        forcegroups = {}
        for i in range(self.OMMsystem.getNumForces()):
            force = self.OMMsystem.getForce(i)
            force.setForceGroup(i)
            forcegroups[force] = i
        return forcegroups

    def get_energies(self):
        from openmm.openmm import KcalPerKJ

        def component_name(k):
            return str(k).split('.')[-1].split(';')[0].lstrip('"')

        energies = {}
        for f, i in self.forcegroups.items():
            en = self.OMMsimulation.context.getState(
                getEnergy=True, groups=2**i).getPotentialEnergy()
            name = component_name(f)
            energies[name] = en._value * KcalPerKJ
        return energies


def omm_system(sys, *ff_list):
    """ Instantiate a omm system from a system and a force field.

    Args:
        *ff_list: list of the force fields to be included,
            in priority order.
            Use the :func:py:`get_available_ff_names` to identify the
            available force fields.
    Returns:
        OMMSystem: Instance ready to be optimized.
    """
    symm = OMMSystem(sys)
    symm.set_forcefields(*ff_list)
    symm.set_integrator()
    return symm


def optimize_system(sys, outfile, *forcefields):
    """Take a BigDFT system and optimize it according to a set of OMM FFs.

    Args:
        sys (BigDFT.System): the system to optimize.
        outfile (str): The file to write the optimized system to.
        *forcefields: sequence of forcefields to be employed in the
             optimization.

    Returns:
        The objects returned by the `py:meth:minimizeEnergy` method of OpenMM.
    """
    symm = omm_system(sys, *forcefields)
    obj = symm.optimize(0)
    symm.write(outfile)
    return obj


def three_point_energies(sys, subs1, subs2, *forcefields):
    """Calculate three_point_energies of two portion of a system.
    Args:
        sys (BigDFT.System): the system to optimize.
        subs1 (list): list of fragments composing subsystem 1
        subs2 (list): list of fragments composing subsystem 2
        *forcefields: sequence of forcefields to be employed in the
             optimization.

    Returns:
        collections.namedtuple: tuple of the three energies, eT, e1, e2.
    """
    from collections import namedtuple
    sys1 = omm_system(sys.subsystem(subs1), *forcefields)
    e1 = sys1.OMMenergy()
    sys2 = omm_system(sys.subsystem(subs2), *forcefields)
    e2 = sys2.OMMenergy()
    sysT = omm_system(sys, *forcefields)
    eT = sysT.OMMenergy()
    ThreePoint = namedtuple('ThreePoint', 'eT e1 e2')
    return ThreePoint(eT, e1, e2)


def get_mutation_name(mut, offset=0):
    """From a usual mutation notation AXXXB get the PDBfixer-compliant tuple.

    Args:
        mut (str): Mutation written in the AxxxB format.
        offset (int): offest to be applied to the ``xxx`` format.

    Returns:
        str: the mutation name.
    """
    from Bio.PDB.Polypeptide import one_to_three
    wt = mut[0]
    mt = mut[-1]
    num = int(mut[1:-1]) - offset
    return '-'.join([one_to_three(wt), str(num), one_to_three(mt)])


def _remove_extremal_missing_residues(fixer):
    chains = list(fixer.topology.chains())
    keys = list(fixer.missingResidues.keys())
    for key in keys:
        chain = chains[key[0]]
        if key[1] == 0 or key[1] == len(list(chain.residues())):
            del fixer.missingResidues[key]


def fixed_system(pH=None, mutations=[], system=None, filename=None, pdbid=None,
                 select_chains=None, remove_extremal_missing_residues=True,
                 custom_fixing_function=None):
    """
    Create a system with the pdbfixer tool.

    Args:
        pH (float): the pH of the environment. Add missing hydrogens according
            to this pH if provided.
        mutations (list): the mutation to be applied to the original structure.
            should be a list of two-element lists, the first being the list
            of mutations to be applied to the chain_id, provided by the second.
        system (BigDFT.System): the system to be fixed.
        filename (str): the PDB file from which the system has to be read.
           If system is provided, this arguments indicate the PDB file in which
           the system is written.
        pdbid (str): the id of the PDB database.
        select_chains (list): if provided indicate the chains to be selected.
            Useful when pdbid is provided.
        remove_extremal_missing_residues (bool): if True only the internal
            missing residues are restored.
        custom_fixing_function (func): a function that has a `BigDFT.System`
            as an argument and returns the object to be provided.
    Returns:
        BigDFT.BioQM.BioSystem: Fixed system, or returned object of
            `custom_fixing_function` if present.
    """
    from pdbfixer import PDBFixer
    from openmm.app import PDBFile
    from BigDFT import IO, BioQM
    from os.path import isfile
    from os import remove
    from futile.Utils import unique_filename

    if system is not None and not isfile(filename):
        IO.write_pdb(ofile=open(filename, 'w'), system=system)
    if filename is not None:
        fixer = PDBFixer(filename=filename)
    else:
        fixer = PDBFixer(pdbid=pdbid)
    if len(mutations) > 0:
        for muts in mutations:
            fixer.applyMutations(*muts)
    if filename is None:
        numChains = len(list(fixer.topology.chains()))
        fixer.removeChains([ch for ch in range(numChains)
                            if ch not in select_chains])
    fixer.findMissingResidues()
    if remove_extremal_missing_residues:
        _remove_extremal_missing_residues(fixer)
    fixer.findNonstandardResidues()
    fixer.replaceNonstandardResidues()
    fixer.findMissingAtoms()
    fixer.addMissingAtoms()
    if pH is not None:
        fixer.addMissingHydrogens(pH)
    tmpfile = unique_filename(prefix='fixer_output_') + '.pdb'
    ofile = open(tmpfile, 'w')
    PDBFile.writeFile(fixer.topology, fixer.positions, ofile)
    ofile.close()
    ofile = open(tmpfile, 'r')
    sys = IO.read_pdb(ofile, include_chain=True, ignore_connectivity=True,
                      ignore_unit_cell=True)
    ofile.close()
    remove(tmpfile)

    if custom_fixing_function is not None:
        resys = custom_fixing_function(sys)
    else:
        resys = BioQM.BioSystem.from_sys(sys)

    return resys
