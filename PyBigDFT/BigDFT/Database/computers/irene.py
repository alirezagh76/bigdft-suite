import yaml
from BigDFT.RemoteRunnerUtils import CallableAttrDict
from BigDFT.URL import URL

irene_spec = """
jobname: NAME
mpi: MPI
omp: OMP
cores_per_node: 128
time: TIME
project: gen12049
filesystem: work,scratch
queue: rome
prefix: $CCCWORKDIR/binaries/gnu8-python3/install
modules:
   - gnu/8.3.0
   - mpi/openmpi/4.0.2
   - mkl
   - intelpython3
   - cmake
submitter: ccc_msub
"""


class IreneScriptPrologue(CallableAttrDict):
    def __str__(self):
        shebang = "#!/bin/bash"
        pragma = "#MSUB"
        options = [('-r', self.jobname),
                   ('-N', self.mpi),
                   ('-c', self.omp),
                   ('-T', self.time),
                   ('-q', self.queue),
                   ('-p', self.project),
                   ('-o', 'job-'+str(self.jobname)+'.o'),
                   ('-e', 'job-'+str(self.jobname)+'.e'),
                   ('-m', self.filesystem)]
        prologue = '\n'.join([shebang] +
                             [' '.join([pragma, opt, str(val)])
                              for opt, val in options] +
                             ['set -x', "cd $BRIDGE_MSUB_PWD"])
        return prologue


class IreneScript(CallableAttrDict):
    def __str__(self):
        prologue = IreneScriptPrologue(self)
        text = '\n'.join(['export PREFIX='+str(self.prefix), 'module purge'] +
                         ['module load '+str(mod) for mod in self.modules] +
                         ['export OMP_NUM_THREADS='+str(self.omp)])
        text += """
source $PREFIX/bin/bigdftvars.sh
export PYTHONPATH=$PREFIX/lib/python3.6/site-packages:$PYTHONPATH
export OMPI_MCA_orte_base_help_aggregate=0
export OMPI_MCA_coll="^ghc,tuned"
export MKL_DEBUG_CPU_TYPE=5
export BIGDFT_MPIRUN='ccc_mprun'
export FUTILE_PROFILING_DEPTH=0
"""
        return '\n'.join([prologue(), text])


submission_script = IreneScript(yaml.load(irene_spec, Loader=yaml.Loader))

url = URL(host='irene')
