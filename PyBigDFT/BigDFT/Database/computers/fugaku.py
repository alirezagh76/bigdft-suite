def local_scr(nodes, ppn, omp, bigdft_prefix, **kwargs):
    top = "#!/bin/sh\n"
    top += '#PJM -L "node=' + str(nodes) + '"\n'
    top += '#PJM -L "rscunit=rscunit_ft01"\n'
    if nodes <= 384:
      top += '#PJM -L "rscgrp=small"\n'
    else:
      top += '#PJM -L "rscgrp=large"\n'
    top += '#PJM -L "elapse=1:00:00"\n'
    top += '#PJM --mpi "max-proc-per-node=4"\n'

    name = kwargs["arguments"]["sys_name"]
    
    middle = 'export FUTILE_PROFILING_DEPTH=0\n'
    middle += 'module load Python3-CN\n'
    middle += 'export FLIB_CNTL_BARRIER_ERR=FALSE\n'
    middle += "source " + bigdft_prefix + "/bin/bigdftvars.sh\n"
    middle += 'export BIGDFT_MPIRUN="mpiexec -std ' + name + '.txt"\n'
    middle += "export OMP_NUM_THREADS=" + str(omp) + "\n"    
    
    return top + middle

BIGDFT_PREFIX='/home/hp200179/u00771/binaries/louis/install' 

runner_args = {}
runner_args["script"] = local_scr
runner_args["skip"] = True
runner_args["verbose"] = False
runner_args["protocol"] = "Dill"

runner_args["remote_dir"] = "/home/hp200179/u00771/runs/remote/benchmark" 
runner_args["local_dir"] = "local_work"
runner_args["url"] = "u00771@fugaku.r-ccs.riken.jp"
runner_args["submitter"] = "pjsub"
runner_args["python"] = "python3"

# Script specifics
runner_args["ppn"] = 4
runner_args["omp"] = 12
runner_args["bigdft_prefix"] = BIGDFT_PREFIX
