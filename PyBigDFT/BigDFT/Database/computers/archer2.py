import os

import yaml
from BigDFT.RemoteRunnerUtils import CallableAttrDict
from BigDFT.URL import URL

archer2_spec = """
jobname: NAME
mpi: MPI
omp: OMP
time: TIME
project: e572
qos: standard
partition: standard
modules:
   - cray-python
submitter: sbatch
"""


class Archer2ScriptPrologue(CallableAttrDict):
    def __str__(self):
        shebang = "#!/bin/bash"
        pragma = "#SBATCH"
        options = [('--job-name', self.jobname),
                   ('--nodes', self.mpi),  # TODO check the order
                   ('--cpus-per-task', self.omp),
                   ('--time', self.time),
                   ('--account', self.project),
                   ('--qos', self.qos),
                   ('--partition', self.partition),
                   ('--export', 'none')]
        prologue = '\n'.join([shebang] +
                             [f'{pragma} {opt}={str(val)}'
                              for opt, val in options])
        return prologue


class Archer2Script(CallableAttrDict):
    def __str__(self):
        prologue = Archer2ScriptPrologue(self)
        sourcepath = f'/work/{self.project}/{self.project}/' \
                     f'{os.environ["USER"]}/build/bigdft/bigdftvars.sh'
        text = '\n'.join(['module load ' + str(mod) for mod in self.modules] +
                         ['export OMP_NUM_THREADS=' + str(self.omp)])
        text += f"""
source {sourcepath}
export FUTILE_PROFILING_DEPTH=0
"""
        return '\n'.join([prologue(), text])


submission_script = Archer2Script(yaml.load(archer2_spec, Loader=yaml.Loader))

url = URL()
