import yaml
from BigDFT.RemoteRunnerUtils import CallableAttrDict

spec = """
jobname: NAME
mpi: MPI
omp: OMP
time: TIME
bindir: $BIGDFT_ROOT
submitter: bash
append_string: ''
"""


class ScriptPrologue(CallableAttrDict):
    def __str__(self):
        shebang = "#!/bin/bash"
        return shebang + '\n'


class LocalScript(CallableAttrDict):
    def __str__(self):
        prologue = ScriptPrologue(self)
        text = [prologue(),
                'export OMP_NUM_THREADS='+str(self.omp),
                'source '+str(self.bindir)+'/bigdftvars.sh',
                'export FUTILE_PROFILING_DEPTH=0',
                'export BIGDFT_MPIRUN="mpirun -np '+str(self.mpi)+'"',
                self.append_string+'\n']
        return '\n'.join(text)


submission_script = LocalScript(yaml.load(spec, Loader=yaml.Loader))
