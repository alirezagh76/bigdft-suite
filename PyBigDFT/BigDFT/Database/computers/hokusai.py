def local_scr(nodes, ppn, key, omp, bigdft_prefix, **kwargs):
    top = "#!/bin/sh\n"
    top += "#PJM -L rscunit=bwmpc\n"
    top += "#PJM -L rscgrp=batch\n"
    top += "#PJM -L vnode=" + str(nodes) + "\n"
    top += "#PJM -L vnode-core=40\n"
    top += "#PJM -L elapse=0:30:00\n"
    top += "#PJM -g " + key + "\n"
    top += "#PJM -j\n"
    
    middle = "source ~/.bashrc\n"
    middle += 'export BIGDFT_MPIRUN="mpirun -np ' + str(nodes*ppn) + ' -ppn ' + str(ppn) + '"\n'
    middle += "source " + bigdft_prefix + "/bin/bigdftvars.sh\n"
    middle += "export OMP_NUM_THREADS=" + str(omp) + "\n"    
    middle += "conda activate remote_bigdft\n"
    
    return top + middle

BIGDFT_PREFIX='/home/wddawson/binaries/louis/install' 

runner_args = {}
runner_args["script"] = local_scr
runner_args["skip"] = True
runner_args["verbose"] = False
runner_args["protocol"] = "Dill"

runner_args["remote_dir"] = "/home/wddawson/runs/louis/benchmark_water"
runner_args["local_dir"] = "local_work"
runner_args["url"] = "wddawson@hokusai.riken.jp"
runner_args["submitter"] = "pjsub"

# Script specifics
# runner_args["nodes"] = 1
runner_args["key"] = "Q21460"
runner_args["ppn"] = 4
runner_args["omp"] = 10
runner_args["bigdft_prefix"] = BIGDFT_PREFIX
