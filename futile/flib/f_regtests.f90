module f_regtests
  implicit none
  private

  interface compare
     module procedure compare_l, compare_i, compare_f, compare_d, compare_s
     module procedure compare_dict, compare_dict_d
     module procedure compare_ai, compare_af, compare_ad
     module procedure compare_ad2
  end interface compare

  public :: run, compare, verify
contains
  subroutine run(func)
    use yaml_output
    use dictionaries
    implicit none
    interface
       subroutine func(test)
         character(len = *), intent(out) :: test
       end subroutine func
    end interface

    character(len = max_field_length) :: test
    type(dictionary), pointer :: errors

    nullify(errors)
    call f_err_open_try()
    call func(test)
    call f_err_close_try(errors)

    call yaml_sequence(advance = "no")
    if (associated(errors)) then
       call yaml_mapping_open(test)
       call yaml_map("status", "failed")
       call yaml_map("error", errors)
       call yaml_mapping_close()
       call dict_free(errors)
    else
       call yaml_map(test, "succeed")
    end if
  end subroutine run

  subroutine verify(val, label)
    use dictionaries
    implicit none
    logical, intent(in) :: val
    character(len = *), intent(in) :: label

    if (.not. val) call f_err_throw(label // ": failed")
  end subroutine verify
  
  subroutine compare_l(val, expected, label)
    use dictionaries
    use yaml_strings
    implicit none
    logical, intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    ok = val .eqv. expected
    include 'compare-scalar-inc.f90'
  end subroutine compare_l

  subroutine compare_i(val, expected, label)
    use dictionaries
    use f_precisions, only: f_integer
    use yaml_strings
    implicit none
    integer(f_integer), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    ok = val /= expected
    include 'compare-scalar-inc.f90'
  end subroutine compare_i

  subroutine compare_f(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_simple
    use yaml_strings
    implicit none
    real(f_simple), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_simple), intent(in), optional :: tol
    logical :: ok
    if (present(tol)) then
       ok = abs(val - expected) < tol
    else
       ok = val == expected
    end if
    include 'compare-scalar-inc.f90'
  end subroutine compare_f

  subroutine compare_d(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    if (present(tol)) then
       ok = abs(val - expected) < tol
    else
       ok = val == expected
    end if
    include 'compare-scalar-inc.f90'
  end subroutine compare_d

  subroutine compare_s(val, expected, label)
    use dictionaries
    use yaml_strings
    implicit none
    character(len = *), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    ok = val == expected
    include 'compare-scalar-inc.f90'
  end subroutine compare_s

  subroutine compare_dict(dict, expected, label)
    use dictionaries
    use yaml_strings
    implicit none
    type(dictionary), pointer :: dict
    character(len = *), intent(in) :: expected
    character(len = *), intent(in), optional :: label
    character(len = max_field_length) :: val

    val = dict
    call compare(val, expected, label)
  end subroutine compare_dict

  subroutine compare_dict_d(dict, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double, f_long
    use yaml_strings
    implicit none
    type(dictionary), pointer :: dict
    real(f_double), intent(in) :: expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    real(f_double) :: val

    val = dict
    call compare(val, expected, label, tol)
  end subroutine compare_dict_d

  subroutine compare_ai(val, expected, label)
    use dictionaries
    use f_precisions, only: f_integer
    use yaml_strings
    implicit none
    integer(f_integer), dimension(:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    ok = all(val == expected)
    include 'compare-array-inc.f90'
  end subroutine compare_ai

  subroutine compare_af(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_simple
    use yaml_strings
    implicit none
    real(f_simple), dimension(:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_simple), intent(in), optional :: tol
    logical :: ok
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_af

  subroutine compare_ad(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), dimension(:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_ad

  subroutine compare_ad2(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), dimension(:,:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_ad2
end module f_regtests
